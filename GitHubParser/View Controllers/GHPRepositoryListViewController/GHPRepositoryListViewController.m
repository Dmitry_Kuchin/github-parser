//
//  GHPRepositoryListViewController.m
//  GitHubParser
//
//  Created by Дмитрий Кучин on 29.03.2018.
//  Copyright © 2018 Дмитрий Кучин. All rights reserved.
//

#import "GHPRepositoryListViewController.h"
#import "GHPRepositoryViewCell.h"

#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.f green:g/255.f blue:b/255.f alpha:a]

@interface GHPRepositoryListViewController () <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic, readwrite) IBOutlet UITableView *tableView;
@property (strong, nonatomic, readwrite, nonnull) UIRefreshControl *refreshControl;
@end

@implementation GHPRepositoryListViewController

#pragma mark - Override

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - Initialization

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.presenter = [GHPRepositoryListPresenter.alloc initWithView:self];
    }
    return self;
}

#pragma mark - Life Cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.presenter interact];
}

#pragma mark - Configure

- (void)decorate {
    self.navigationItem.title = (self.typeController == GHPRepositoryListViewControllerTypeRepository) ? @"Репозитории" : self.currentRepository.name;
    self.navigationController.navigationBar.backgroundColor = RGBA(255, 45, 85, 1);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = RGBA(235, 237, 240, 1);
    self.tableView.separatorStyle = UITableViewCellSelectionStyleGray;
    self.tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 0);
    self.tableView.estimatedRowHeight = 114.f;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.refreshControl = [UIRefreshControl.alloc init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    [self.refreshControl addTarget:self action:@selector(update) forControlEvents:UIControlEventValueChanged];
    [self.refreshControl layoutIfNeeded];
    [self.tableView insertSubview:self.refreshControl atIndex:0];
    UINib *settingsTableViewCellNib = [UINib nibWithNibName:NSStringFromClass(GHPRepositoryViewCell.class) bundle:nil];
    [self.tableView registerNib:settingsTableViewCellNib forCellReuseIdentifier:NSStringFromClass(GHPRepositoryViewCell.class)];
    [self.presenter fetchDataWithCompletion:^(BOOL success, NSError * _Nullable error) {} typeController:self.typeController];
    if (self.typeController == GHPRepositoryListViewControllerTypeDetail) {
        [self.presenter configureCurrentRepository:self.currentRepository];
    }
}

- (void)insertRowsFromIndexPath:(NSArray<NSIndexPath *> *)indexPaths {
//    CGFloat bottomOffset = _tableView.contentOffset.y;
//    [self.tableView beginUpdates];
//    [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
//    [self.tableView endUpdates];
//    [self.tableView setContentOffset:CGPointMake(0, bottomOffset) animated:NO];
    [self.tableView reloadData];
}

- (void)updateTableView {
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
}

#pragma mark - Actions
- (void)update {
    [self.presenter update];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.typeController == GHPRepositoryListViewControllerTypeDetail) { return; }
    GHPRepository *repository = [self.presenter objectAtIndex:indexPath.row];
    GHPRepositoryListViewController *viewController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:NSStringFromClass(GHPRepositoryListViewController.class)];
    viewController.currentRepository = repository;
    viewController.typeController = GHPRepositoryListViewControllerTypeDetail;
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.presenter.contentCount.integerValue - 1 && self.presenter.isFecthig) {
        [self.presenter fetchDataWithCompletion:^(BOOL success, NSError * _Nullable error) {
            error ? [self showErrorState] : [self hideErrorState];
//            if (error) {
//                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(15.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//                    [self tableView:tableView willDisplayCell:cell forRowAtIndexPath:indexPath];
//                });
//            }
        } typeController:self.typeController];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.presenter.contentCount.integerValue;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GHPRepositoryViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass(GHPRepositoryViewCell.class)];
    if (self.typeController == GHPRepositoryListViewControllerTypeRepository) {
        [cell setupCellWithModel:[self.presenter objectAtIndex:indexPath.row]];
    } else {
        [cell setupCellWithCommit:[self.presenter objectAtIndex:indexPath.row]];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


#pragma mark - GHPErrorable
- (void)hideErrorState { 
    self.tableView.tableFooterView = UIView.new;
}

- (void)showErrorState { 
    self.tableView.tableFooterView = self.createErrorView;
}

- (UIView *)createErrorView {
    UIView *backgroundView = [UIView.alloc initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 50.f)];
    UILabel *errorLabel = [UILabel.alloc initWithFrame:backgroundView.frame];
    errorLabel.attributedText = [NSAttributedString.alloc initWithString:@"Произошла ошибка, попробуйте позже" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:13.f],
                     NSForegroundColorAttributeName : RGBA(140, 140, 140, 1)}];
    [backgroundView addSubview:errorLabel];
    
    return backgroundView;
    
}

#pragma mark - GHPLoadingable
- (void)showLoadingState {
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 50)];
    backgroundView.backgroundColor = UIColor.clearColor;
    UIActivityIndicatorView *profileActivityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [profileActivityIndicator startAnimating];
    [profileActivityIndicator setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2 - profileActivityIndicator.frame.size.width / 2, 8, profileActivityIndicator.frame.size.width, profileActivityIndicator.frame.size.height)];
    [backgroundView addSubview:profileActivityIndicator];
    
    self.tableView.tableFooterView = backgroundView;
}

- (void)hideLoadingState {
    self.tableView.tableFooterView = UIView.new;
}

@end
