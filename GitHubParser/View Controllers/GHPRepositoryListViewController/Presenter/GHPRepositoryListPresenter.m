//
//  GHPRepositoryListPresenter.m
//  GitHubParser
//
//  Created by Дмитрий Кучин on 30.03.2018.
//  Copyright © 2018 Дмитрий Кучин. All rights reserved.
//

#import "GHPRepositoryListPresenter.h"
#import "GHPNetworkManager.h"
#import <UIKit/UIKit.h>

@interface GHPRepositoryListPresenter ()
@property (strong, nonatomic, readwrite, nullable) NSMutableArray *content;
@property (strong, nonatomic, readwrite, nonnull) NSNumber *_contentCount;
@property (strong, nonatomic, readwrite, nonnull) NSNumber *_count;
@property (strong, nonatomic, readwrite, nonnull) NSNumber *_page;
@property (assign, nonatomic, readwrite) BOOL _isFetchig;
@property (assign, nonatomic, readwrite) BOOL isUpdating;

@property (assign, nonatomic, readwrite) GHPRepositoryListViewControllerType typeController;

@property (strong, nonatomic, readwrite) dispatch_queue_t fecthRepositoryQueue;
@end

@implementation GHPRepositoryListPresenter

#pragma mark - Initialization

- (instancetype)initWithView:(id<GHPRepositoryView>)view {
    self = [super init];
    if (self) {
        self.view = view;
    }
    return self;
}


- (NSNumber *)contentCount {
    return @(self.content.count);
}

- (NSNumber *)count {
    return self._count;
}

- (NSNumber *)page {
    return self._page;
}

- (BOOL)isFecthig {
    return self._isFetchig;
}

- (nullable id)objectAtIndex:(NSInteger)index {
    return self.content.count > 0 ? [self.content objectAtIndex:index] : nil;
}

- (void)interact {
    self.content = NSMutableArray.array;
    self._count = @20;
    self._page = @1;
    self._isFetchig = YES;
    self.isUpdating = NO;
    self.fecthRepositoryQueue = dispatch_queue_create("com.example.GitHubParse.repositoryPresenter.fetch", NULL);
    
    [self.view decorate];
}

- (void)update {
    self.content = NSMutableArray.array;
    self._count = @20;
    self._page = @1;
    self._isFetchig = YES;
    self.isUpdating = YES;
    [self fetchDataWithCompletion:^(BOOL success, NSError * _Nullable error) {} typeController:self.typeController];
}

- (void)configureCurrentRepository:(nonnull GHPRepository *)repository {
    self.repository = repository;
}

- (void)fetchDataWithCompletion:(void (^_Nullable)(BOOL success, NSError * _Nullable error))completionHandler typeController:(NSInteger)typeController {
    self.typeController = typeController;
    self._isFetchig = NO;
    switch (typeController) {
        case GHPRepositoryListViewControllerTypeRepository: {
            [self fetchRepositoryFromServerWithCompletion:^(BOOL success, NSError * _Nullable error) {
                completionHandler(success, error);
            }];
           break;
        }
        case GHPRepositoryListViewControllerTypeDetail: {
            [self fetchInfoRepositoryFromServerWithCompletion:^(BOOL success, NSError * _Nullable error) {
                completionHandler(success, error);
            }];
            break;
        }
    }
}

- (void)fetchRepositoryFromServerWithCompletion:(void (^_Nullable)(BOOL success, NSError * _Nullable error))completionHandler {
    [self.view showLoadingState];
    dispatch_async(self.fecthRepositoryQueue, ^{
        [GHPNetworkManager.sharedInstance fecthRepositoryFromServerWithCount:self._count query:@"language:swift" page:self._page sort:@"stars" order:GHPNetworkManagerOrderTypeDesc completionHandler:^(NSArray<GHPRepository *> * _Nullable items, NSError * _Nullable error) {
            [self safeCallLoadingState];
            if (error) {
                self._isFetchig = NO;
                completionHandler(NO, error);
                return;
            }
            [self.content addObjectsFromArray:items];
            self._page = @(self._page.integerValue + 1);
            NSMutableArray<NSIndexPath *> *indexPathArray = NSMutableArray.array;
            for (int i = 0; i < items.count; i++) {
                [indexPathArray addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            }
            if (self.isUpdating) {
                [self.view updateTableView];
            } else {
                [self.view insertRowsFromIndexPath:indexPathArray];
            }
            self._isFetchig = YES;
            completionHandler(YES, nil);
        }];
    });
}

- (void)fetchInfoRepositoryFromServerWithCompletion:(void (^_Nullable)(BOOL success, NSError * _Nullable error))completionHandler {
    [self.view showLoadingState];
    dispatch_async(self.fecthRepositoryQueue, ^{
        [GHPNetworkManager.sharedInstance fetchInfoRepositoryFromServerWithCount:self._count page:self._page fullName:self.repository.fullname completionHandler:^(NSArray<GHPParentCommit *> * _Nullable items, NSError * _Nullable error) {
            [self safeCallLoadingState];
            if (error) {
                self._isFetchig = YES;
                completionHandler(NO, error);
                return;
            }
            [self.content addObjectsFromArray:items];
            self._page = @(self._page.integerValue + 1);
            NSMutableArray<NSIndexPath *> *indexPathArray = NSMutableArray.array;
            for (int i = 0; i < items.count; i++) {
                [indexPathArray addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            }
            if (self.isUpdating) {
                [self.view updateTableView];
            } else {
                [self.view insertRowsFromIndexPath:indexPathArray];
            }
            self._isFetchig = YES;
            completionHandler(YES, nil);
        }];
    });
}

#pragma mark - Support

- (void)safeCallLoadingState {
    if (NSThread.isMainThread) { [self.view showLoadingState]; }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.view showLoadingState];
    });
}

@end
