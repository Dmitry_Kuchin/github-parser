//
//  GHPRepositoryListPresenter.h
//  GitHubParser
//
//  Created by Дмитрий Кучин on 30.03.2018.
//  Copyright © 2018 Дмитрий Кучин. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GHPRepositoryListViewControllerPresenter.h"
#import "GHPRepositoryView.h"

typedef NS_ENUM(NSInteger, GHPRepositoryListViewControllerType) {
    GHPRepositoryListViewControllerTypeRepository,
    GHPRepositoryListViewControllerTypeDetail
};

@interface GHPRepositoryListPresenter : NSObject <GHPRepositoryListViewControllerPresenter>

- (instancetype _Nonnull )initWithView:(id<GHPRepositoryView> _Nonnull)view;

@property (weak, nonatomic, readwrite, nullable) id <GHPRepositoryView> view;
@property (strong, nonatomic, readwrite, nullable) GHPRepository *repository;

@end
