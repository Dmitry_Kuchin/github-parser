//
//  GHPRepositoryListViewControllerPresenter.h
//  GitHubParser
//
//  Created by Дмитрий Кучин on 30.03.2018.
//  Copyright © 2018 Дмитрий Кучин. All rights reserved.
//

#import "MVP.h"
#import "GHPRepository+CoreDataClass.h"

@protocol GHPRepositoryListViewControllerPresenter <GHPPresenter>
@property (strong, nonatomic, readonly, nonnull) NSNumber *contentCount;
@property (strong, nonatomic, readonly, nonnull) NSNumber *count;
@property (strong, nonatomic, readonly, nonnull) NSNumber *page;

@property (assign, nonatomic, readonly) BOOL isFecthig;

- (void)fetchDataWithCompletion:(void (^_Nullable)(BOOL success, NSError * _Nullable error))completionHandler typeController:(NSInteger)typeController;

- (nullable id)objectAtIndex:(NSInteger)index;
- (void)configureCurrentRepository:(nonnull GHPRepository *)repository;

@end
