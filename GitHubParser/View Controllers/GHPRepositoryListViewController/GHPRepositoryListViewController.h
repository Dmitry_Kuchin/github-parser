//
//  GHPRepositoryListViewController.h
//  GitHubParser
//
//  Created by Дмитрий Кучин on 29.03.2018.
//  Copyright © 2018 Дмитрий Кучин. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GHPRepositoryListViewControllerPresenter.h"
#import "GHPRepositoryListPresenter.h"
#import "GHPRepositoryView.h"

@interface GHPRepositoryListViewController : UIViewController <GHPRepositoryView>
@property (strong, nonatomic, readwrite, nullable) id <GHPRepositoryListViewControllerPresenter> presenter;
@property (assign, nonatomic, readwrite) GHPRepositoryListViewControllerType typeController;

@property (strong, nonatomic, readwrite, nullable) GHPRepository *currentRepository;
@end
