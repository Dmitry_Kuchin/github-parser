//
//  GHPRepositoryView.h
//  GitHubParser
//
//  Created by Дмитрий Кучин on 30.03.2018.
//  Copyright © 2018 Дмитрий Кучин. All rights reserved.
//

#import "MVP.h"
@protocol GHPRepositoryView <GHPPresentableView, GHPErrorable, GHPLoadingable>
- (void)insertRowsFromIndexPath:(NSArray<NSIndexPath *> *)indexPaths;
- (void)updateTableView;
@end
