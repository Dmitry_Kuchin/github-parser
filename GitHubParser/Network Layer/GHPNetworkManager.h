//
//  GHPNetworkManager.h
//  GitHubParser
//
//  Created by Дмитрий Кучин on 30.03.2018.
//  Copyright © 2018 Дмитрий Кучин. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GHPDataManager.h"

extern const NSString * _Nonnull PATHSERVER;
extern const NSString * _Nonnull SEARCH;
extern const NSString * _Nonnull REPOSITORIES;
extern const NSString * _Nonnull REPOS;
extern const NSString * _Nonnull COMMITS;

@class Reachability;

typedef NS_ENUM(NSInteger, GHPNetworkManagerOrderType) {
    GHPNetworkManagerOrderTypeAsc,
    GHPNetworkManagerOrderTypeDesc
};

@interface GHPNetworkManager : NSObject

@property (strong, nonatomic, readonly, nonnull) Reachability *reachability;

+ (nonnull GHPNetworkManager *)sharedInstance;

- (void)fecthRepositoryFromServerWithCount:(nonnull NSNumber *)count query:(nonnull NSString *)query page:(nonnull NSNumber *)page sort:(nonnull NSString *)sort order:(GHPNetworkManagerOrderType)order completionHandler:(void(^ _Nullable)(NSArray<GHPRepository *> * _Nullable items, NSError * _Nullable error))completionHandler;
- (void)fetchInfoRepositoryFromServerWithCount:(nonnull NSNumber *)count page:(nonnull NSNumber *)page fullName:(nonnull NSString *)fullName completionHandler:(void(^ _Nullable)(NSArray<GHPParentCommit *> * _Nullable items, NSError * _Nullable error))completionHandler;

@end
