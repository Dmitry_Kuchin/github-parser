//
//  GHPNetworkManager.m
//  GitHubParser
//
//  Created by Дмитрий Кучин on 30.03.2018.
//  Copyright © 2018 Дмитрий Кучин. All rights reserved.
//

#import "GHPNetworkManager.h"
#import <AFNetworking/AFNetworking.h>
#import <MagicalRecord/MagicalRecord.h>
#import "Reachability.h"

NSString *PATHSERVER = @"https://api.github.com/";
NSString *SEARCH = @"search/";
NSString *REPOSITORIES = @"repositories";
NSString *REPOS = @"repos/";
NSString *COMMITS = @"/commits";

@interface GHPNetworkManager ()
@property (strong, nonatomic, readwrite, nonnull) Reachability *reachability;
@end

@implementation GHPNetworkManager

#pragma mark - Singleton
+ (GHPNetworkManager *)sharedInstance {
    static dispatch_once_t onceToken;
    static GHPNetworkManager *networkManager = nil;
    dispatch_once(&onceToken, ^{
        networkManager = [GHPNetworkManager.alloc init];
        networkManager.reachability = [Reachability reachabilityForInternetConnection];
        [networkManager.reachability startNotifier];
    });
    return networkManager;
}

#pragma mark - Server Method
- (void)fecthRepositoryFromServerWithCount:(NSNumber *)count query:(NSString *)query page:(NSNumber *)page sort:(NSString *)sort order:(GHPNetworkManagerOrderType)order completionHandler:(void(^ _Nullable)(NSArray<GHPRepository *> * _Nullable items, NSError * _Nullable error))completionHandler {
    NSDictionary *parameters = @{@"per_page" : count,
                                 @"page" :  page,
                                 @"q" : query,
                                 @"sort" : sort,
                                 @"order" : [self orderString:order]
                                 };
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:[NSString stringWithFormat:@"%@%@%@", PATHSERVER, SEARCH, REPOSITORIES] parameters:parameters error:nil];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [AFURLSessionManager.alloc initWithSessionConfiguration:configuration];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request uploadProgress:^(NSProgress * _Nonnull uploadProgress) {} downloadProgress:^(NSProgress * _Nonnull downloadProgress) {} completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if (error) {
            completionHandler(nil, error);
        }
        NSDictionary *dictionary = responseObject;
        if (![dictionary[@"items"] isKindOfClass:NSArray.class]) { completionHandler(nil, error); }
        NSArray<NSDictionary *> *items = dictionary[@"items"];
        NSArray<GHPRepository *> *result = [GHPDataManager.sharedInstance createRepositoryEntityFromArray:items];
        completionHandler(result, nil);
        
    }];
    [dataTask resume];
}

- (void)fetchInfoRepositoryFromServerWithCount:(nonnull NSNumber *)count page:(nonnull NSNumber *)page fullName:(NSString *)fullName completionHandler:(void(^ _Nullable)(NSArray<GHPParentCommit *> * _Nullable items, NSError * _Nullable error))completionHandler {
    NSDictionary *parameters = @{@"per_page" : count,
                                 @"page" : page};
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"GET" URLString:[NSString stringWithFormat:@"%@%@%@%@", PATHSERVER, REPOS, fullName, COMMITS] parameters:parameters error:nil];
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [AFURLSessionManager.alloc initWithSessionConfiguration:configuration];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request uploadProgress:^(NSProgress * _Nonnull uploadProgress) {} downloadProgress:^(NSProgress * _Nonnull downloadProgress) {} completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if (error) {
            completionHandler(nil, error);
        }
        NSArray<NSDictionary *> *items = responseObject;
        if (![items isKindOfClass:NSArray.class]) { completionHandler(nil, error); }
        NSArray<GHPParentCommit *> *result = [GHPDataManager.sharedInstance createCommitEntityFromArray:items];
        completionHandler(result, nil);
        
    }];
    [dataTask resume];
}

- (BOOL)isNotInternet {
    NetworkStatus status = [self.reachability currentReachabilityStatus];
    return status == NotReachable;
}

#pragma mark - Support
- (NSString *)orderString:(GHPNetworkManagerOrderType)orderValue {
    return orderValue == GHPNetworkManagerOrderTypeAsc ? @"asc" : @"desc";
}

@end
