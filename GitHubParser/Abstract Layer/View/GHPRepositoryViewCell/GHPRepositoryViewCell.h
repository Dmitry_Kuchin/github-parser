//
//  GHPRepositoryViewCell.h
//  GitHubParser
//
//  Created by Дмитрий Кучин on 29.03.2018.
//  Copyright © 2018 Дмитрий Кучин. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GHPRepository;
@class GHPParentCommit;
@interface GHPRepositoryViewCell : UITableViewCell
- (void)setupCellWithModel:(GHPRepository *)model;
- (void)setupCellWithCommit:(GHPParentCommit *)commit;
@end
