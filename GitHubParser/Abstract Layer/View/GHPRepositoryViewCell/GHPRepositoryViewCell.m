//
//  GHPRepositoryViewCell.m
//  GitHubParser
//
//  Created by Дмитрий Кучин on 29.03.2018.
//  Copyright © 2018 Дмитрий Кучин. All rights reserved.
//

#import "GHPRepositoryViewCell.h"
#import "GHPDataManager.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface GHPRepositoryViewCell ()

@property (weak, nonatomic, readwrite) IBOutlet UIView *backgroundForSymbolLabel;

@property (weak, nonatomic, readwrite) IBOutlet UILabel *starsCountLabel;
@property (weak, nonatomic, readwrite) IBOutlet UILabel *firstSymbolFromNameLabel;
@property (weak, nonatomic, readwrite) IBOutlet UILabel *authorLabel;
@property (weak, nonatomic, readwrite) IBOutlet UILabel *nameRepositoryLabel;
@property (weak, nonatomic, readwrite) IBOutlet UILabel *infoRepositoryLabel;

@property (weak, nonatomic, readwrite) IBOutlet UIImageView *avatarProfile;
@property (weak, nonatomic, readwrite) IBOutlet UIImageView *starImageView;

@end

@implementation GHPRepositoryViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundForSymbolLabel.clipsToBounds = YES;
    self.backgroundForSymbolLabel.layer.cornerRadius = CGRectGetWidth(self.backgroundForSymbolLabel.frame) / 2;
    self.avatarProfile.clipsToBounds = YES;
    self.avatarProfile.layer.cornerRadius = CGRectGetWidth(self.backgroundForSymbolLabel.frame) / 2;
}


- (void)setupCellWithModel:(GHPRepository *)model {
    self.authorLabel.text = model.owner.login;
    self.nameRepositoryLabel.text = model.name;
    self.infoRepositoryLabel.text = model.descriptionRepository;
    self.firstSymbolFromNameLabel.text = [[model.name substringToIndex:1] uppercaseString];
    self.starsCountLabel.text = [self convertCounterToString:@(model.stars)];
    self.avatarProfile.hidden = YES;
}

- (void)setupCellWithCommit:(GHPParentCommit *)commit {
    self.authorLabel.text = commit.sha;
    self.nameRepositoryLabel.text = commit.author.login;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Имя: %@\nВетка: %@", commit.commit.message, commit.commit.tree.sha]];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17.f weight:UIFontWeightSemibold] range:NSMakeRange(0, 4)];
    [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:17.f weight:UIFontWeightSemibold] range:NSMakeRange(commit.commit.message.length + 6, 6)];
    self.infoRepositoryLabel.attributedText = attributedString;
    

    self.backgroundForSymbolLabel.hidden = YES;
    self.starImageView.hidden = YES;
    [self.avatarProfile sd_setImageWithURL:[NSURL URLWithString:commit.author.avatar_url]];
    
}

- (NSString *)convertCounterToString:(NSNumber *)value {
    CGFloat floatValue = value.floatValue;
    if (value.integerValue > 1000000) {
        return [[NSString stringWithFormat:@"%.1fM", floatValue/1000000] stringByReplacingOccurrencesOfString:@"." withString:@","];
    } else if (value.integerValue > 1000) {
        return [[NSString stringWithFormat:@"%.1fK", floatValue/1000] stringByReplacingOccurrencesOfString:@"." withString:@","];
    }
    
    return [NSString stringWithFormat:@"%@",value];
}
@end
