//
//  GHPPresentableView.h
//  GitHubParser
//
//  Created by Дмитрий Кучин on 29.03.2018.
//  Copyright © 2018 Дмитрий Кучин. All rights reserved.
//

@protocol GHPPresentableView <NSObject>
- (void)decorate;
@end
