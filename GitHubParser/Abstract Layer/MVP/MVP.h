//
//  MVP.h
//  GitHubParser
//
//  Created by Дмитрий Кучин on 29.03.2018.
//  Copyright © 2018 Дмитрий Кучин. All rights reserved.
//

#ifndef MVP_h
#define MVP_h
#import "GHPPresenter.h"
#import "GHPErrorable.h"
#import "GHPPresentableView.h"
#import "GHPLoadingable.h"

#endif /* MVP_h */
