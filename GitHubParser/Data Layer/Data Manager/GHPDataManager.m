//
//  GHPDataManager.m
//  GitHubParser
//
//  Created by Дмитрий Кучин on 30.03.2018.
//  Copyright © 2018 Дмитрий Кучин. All rights reserved.
//

#import "GHPDataManager.h"
#import <MagicalRecord/MagicalRecord.h>

@implementation GHPDataManager

#pragma mark - Singleton
+ (GHPDataManager *)sharedInstance {
    static dispatch_once_t onceToken;
    static GHPDataManager *dataManager = nil;
    dispatch_once(&onceToken, ^{
        dataManager = [GHPDataManager.alloc init];
    });
    return dataManager;
}

- (NSArray<GHPRepository *> *)createRepositoryEntityFromArray:(NSArray<NSDictionary *> *)array {
    __block NSArray *result = [GHPRepository MR_importFromArray:array];
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext * _Nonnull localContext) {
        [GHPRepository MR_importFromArray:array inContext:localContext];
    }];
    return result;
}

- (NSArray<GHPParentCommit *> *)createCommitEntityFromArray:(NSArray *)array {
    __block NSArray *result = [GHPParentCommit MR_importFromArray:array];
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext * _Nonnull localContext) {
        [GHPParentCommit MR_importFromArray:array inContext:localContext];
    }];
    return result;
}

- (GHPUser *)createUserEntityFromDictionaery:(NSDictionary *)dictionary {
    GHPUser *user = [GHPUser MR_createEntity];
    [user MR_importValuesForKeysWithObject:dictionary];
    return user;
}

- (GHPRepository *)createRepositoryEntityFromDictionaery:(NSDictionary *)dictionary {
    GHPRepository *repository = [GHPRepository MR_createEntity];
    [repository MR_importValuesForKeysWithObject:dictionary];
    return repository;
}

@end
