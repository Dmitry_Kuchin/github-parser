//
//  GHPDataManager.h
//  GitHubParser
//
//  Created by Дмитрий Кучин on 30.03.2018.
//  Copyright © 2018 Дмитрий Кучин. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GHPRepository+CoreDataClass.h"
#import "GHPUser+CoreDataProperties.h"
#import "GHPTree+CoreDataClass.h"
#import "GHPCommit+CoreDataClass.h"
#import "GHPParentCommit+CoreDataClass.h"

@interface GHPDataManager : NSObject
+ (GHPDataManager *)sharedInstance;

- (GHPRepository *)createRepositoryEntityFromDictionaery:(NSDictionary *)dictionary;
- (NSArray<GHPRepository *> *)createRepositoryEntityFromArray:(NSArray<NSDictionary *> *)array;

- (NSArray<GHPParentCommit *> *)createCommitEntityFromArray:(NSArray *)array;

- (GHPUser *)createUserEntityFromDictionaery:(NSDictionary *)dictionary;

@end
