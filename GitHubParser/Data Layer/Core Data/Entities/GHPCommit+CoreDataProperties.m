//
//  GHPCommit+CoreDataProperties.m
//  
//
//  Created by Дмитрий Кучин on 31.03.2018.
//
//

#import "GHPCommit+CoreDataProperties.h"

@implementation GHPCommit (CoreDataProperties)

+ (NSFetchRequest<GHPCommit *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"GHPCommit"];
}

@dynamic message;
@dynamic tree;
@dynamic parent;

@end
