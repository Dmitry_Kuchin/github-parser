//
//  GHPRepository+CoreDataProperties.h
//  
//
//  Created by Дмитрий Кучин on 31.03.2018.
//
//

#import "GHPRepository+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface GHPRepository (CoreDataProperties)

+ (NSFetchRequest<GHPRepository *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *descriptionRepository;
@property (nullable, nonatomic, copy) NSString *fullname;
@property (nonatomic) int32_t identifier;
@property (nullable, nonatomic, copy) NSString *name;
@property (nonatomic) int32_t stars;
@property (nullable, nonatomic, retain) GHPUser *owner;

@end

NS_ASSUME_NONNULL_END
