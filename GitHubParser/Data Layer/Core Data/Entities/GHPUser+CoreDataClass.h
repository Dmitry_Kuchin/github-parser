//
//  GHPUser+CoreDataClass.h
//  
//
//  Created by Дмитрий Кучин on 31.03.2018.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class GHPParentCommit, GHPRepository;

NS_ASSUME_NONNULL_BEGIN

@interface GHPUser : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "GHPUser+CoreDataProperties.h"
