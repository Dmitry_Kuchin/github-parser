//
//  GHPParentCommit+CoreDataProperties.h
//  
//
//  Created by Дмитрий Кучин on 31.03.2018.
//
//

#import "GHPParentCommit+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface GHPParentCommit (CoreDataProperties)

+ (NSFetchRequest<GHPParentCommit *> *)fetchRequest;

@property (nonatomic) int32_t gHPParentCommitID;
@property (nullable, nonatomic, copy) NSString *sha;
@property (nullable, nonatomic, retain) GHPUser *author;
@property (nullable, nonatomic, retain) GHPCommit *commit;

@end

NS_ASSUME_NONNULL_END
