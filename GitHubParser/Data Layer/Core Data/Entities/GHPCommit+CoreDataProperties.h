//
//  GHPCommit+CoreDataProperties.h
//  
//
//  Created by Дмитрий Кучин on 31.03.2018.
//
//

#import "GHPCommit+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface GHPCommit (CoreDataProperties)

+ (NSFetchRequest<GHPCommit *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *message;
@property (nullable, nonatomic, retain) GHPTree *tree;
@property (nullable, nonatomic, retain) GHPParentCommit *parent;

@end

NS_ASSUME_NONNULL_END
