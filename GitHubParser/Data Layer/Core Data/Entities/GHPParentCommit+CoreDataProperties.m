//
//  GHPParentCommit+CoreDataProperties.m
//  
//
//  Created by Дмитрий Кучин on 31.03.2018.
//
//

#import "GHPParentCommit+CoreDataProperties.h"

@implementation GHPParentCommit (CoreDataProperties)

+ (NSFetchRequest<GHPParentCommit *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"GHPParentCommit"];
}

@dynamic gHPParentCommitID;
@dynamic sha;
@dynamic author;
@dynamic commit;

@end
