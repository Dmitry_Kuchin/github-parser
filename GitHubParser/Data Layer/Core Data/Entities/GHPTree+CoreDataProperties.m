//
//  GHPTree+CoreDataProperties.m
//  
//
//  Created by Дмитрий Кучин on 31.03.2018.
//
//

#import "GHPTree+CoreDataProperties.h"

@implementation GHPTree (CoreDataProperties)

+ (NSFetchRequest<GHPTree *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"GHPTree"];
}

@dynamic sha;
@dynamic url;
@dynamic commit;

@end
