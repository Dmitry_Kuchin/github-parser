//
//  GHPTree+CoreDataClass.h
//  
//
//  Created by Дмитрий Кучин on 31.03.2018.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class GHPCommit;

NS_ASSUME_NONNULL_BEGIN

@interface GHPTree : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "GHPTree+CoreDataProperties.h"
