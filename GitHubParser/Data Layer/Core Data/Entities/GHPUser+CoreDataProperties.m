//
//  GHPUser+CoreDataProperties.m
//  
//
//  Created by Дмитрий Кучин on 31.03.2018.
//
//

#import "GHPUser+CoreDataProperties.h"

@implementation GHPUser (CoreDataProperties)

+ (NSFetchRequest<GHPUser *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"GHPUser"];
}

@dynamic avatar_url;
@dynamic identifier;
@dynamic login;
@dynamic repository;
@dynamic commit;

@end
