//
//  GHPRepository+CoreDataProperties.m
//  
//
//  Created by Дмитрий Кучин on 31.03.2018.
//
//

#import "GHPRepository+CoreDataProperties.h"

@implementation GHPRepository (CoreDataProperties)

+ (NSFetchRequest<GHPRepository *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"GHPRepository"];
}

@dynamic descriptionRepository;
@dynamic fullname;
@dynamic identifier;
@dynamic name;
@dynamic stars;
@dynamic owner;

@end
