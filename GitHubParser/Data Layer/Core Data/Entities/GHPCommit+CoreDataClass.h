//
//  GHPCommit+CoreDataClass.h
//  
//
//  Created by Дмитрий Кучин on 31.03.2018.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class GHPParentCommit, GHPTree;

NS_ASSUME_NONNULL_BEGIN

@interface GHPCommit : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "GHPCommit+CoreDataProperties.h"
