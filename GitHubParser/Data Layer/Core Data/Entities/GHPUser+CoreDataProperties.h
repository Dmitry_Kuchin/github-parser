//
//  GHPUser+CoreDataProperties.h
//  
//
//  Created by Дмитрий Кучин on 31.03.2018.
//
//

#import "GHPUser+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface GHPUser (CoreDataProperties)

+ (NSFetchRequest<GHPUser *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *avatar_url;
@property (nonatomic) int32_t identifier;
@property (nullable, nonatomic, copy) NSString *login;
@property (nullable, nonatomic, retain) GHPRepository *repository;
@property (nullable, nonatomic, retain) NSSet<GHPParentCommit *> *commit;

@end

@interface GHPUser (CoreDataGeneratedAccessors)

- (void)addCommitObject:(GHPParentCommit *)value;
- (void)removeCommitObject:(GHPParentCommit *)value;
- (void)addCommit:(NSSet<GHPParentCommit *> *)values;
- (void)removeCommit:(NSSet<GHPParentCommit *> *)values;

@end

NS_ASSUME_NONNULL_END
