//
//  GHPRepository+CoreDataClass.h
//  
//
//  Created by Дмитрий Кучин on 31.03.2018.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class GHPUser;

NS_ASSUME_NONNULL_BEGIN

@interface GHPRepository : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "GHPRepository+CoreDataProperties.h"
