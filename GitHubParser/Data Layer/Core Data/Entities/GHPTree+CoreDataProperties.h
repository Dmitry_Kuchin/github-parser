//
//  GHPTree+CoreDataProperties.h
//  
//
//  Created by Дмитрий Кучин on 31.03.2018.
//
//

#import "GHPTree+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface GHPTree (CoreDataProperties)

+ (NSFetchRequest<GHPTree *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *sha;
@property (nullable, nonatomic, copy) NSString *url;
@property (nullable, nonatomic, retain) GHPCommit *commit;

@end

NS_ASSUME_NONNULL_END
